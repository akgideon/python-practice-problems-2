# Complete the is_divisible_by_3 function to return the
# word "fizz" if the value in the number parameter is
# divisible by 3. Otherwise, just return the number.
#
# You can use the test number % 3 == 0 to test if a
# number is divisible by 3.
#
# Pseudocode is there to guide you.

def is_divisible_by_3(number):
    # If number is divisible by 3
    if (number % 3 == 0):
        return "fizz"
    else:
        return number
        # return "fizz"
    # Otherwise
        # return the number
    pass
